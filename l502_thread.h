//---------------------------------------------------------------------------

#ifndef l502_threadH
#define l502_threadH

#include "l502api.h"

#include <vcl.h>


class L502_ProcessThread : public TThread
{

protected:
    void __fastcall Execute();

public:
    bool stop;  //������ �� ������� (��������������� �� ��������� ������)
    int err;  //��� ������ ��� ���������� ������ �����

    t_l502_hnd hnd; //��������� ������
    TEdit* lchResEdits[L502_LTABLE_MAX_CH_CNT];
    TEdit* dinResEdit;

    __fastcall L502_ProcessThread(bool CreateSuspended);

private:
     void __fastcall updateData() ;
     double* adcData;
     uint32_t firstLch; /* ����� ������� ����������� ������ � ������ ��� */
     uint32_t adcSize;
     uint32_t* dinData;
     uint32_t dinSize;

};



//---------------------------------------------------------------------------
#endif
