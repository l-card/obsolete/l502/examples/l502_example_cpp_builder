//---------------------------------------------------------------------------

#include <vcl.h>
#pragma hdrstop

#include "unit.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma resource "*.dfm"
TForm1 *Form1;

/* ����� ���������� ����� ����������� �������� ���������, � ����� ��������� */
void TForm1::updateControls()
{
    /* ������ �������� ������ �������� ���� �� ������� ���������� */
    btnRefreshDeviceList->Enabled = (hnd==NULL);
    cbbSerialList->Enabled = (hnd==NULL);

    btnOpen->Caption = hnd == NULL ? "���������� ����� � �������" :
                        "��������� ����� � �������";
    btnOpen->Enabled = (hnd!=NULL) || (cbbSerialList->ItemIndex >= 0);

	chkSyncDin->Enabled = (hnd!=NULL) && !threadRunning;
	btnStart->Enabled = (hnd!=NULL) && !threadRunning;
    btnStop->Enabled = (hnd!=NULL) && threadRunning;

    btnSetAdcFreq->Enabled = (hnd!=NULL) && !threadRunning;

    btnAsyncDigOut->Enabled = (hnd!=NULL);
    btnAsyncDigIn->Enabled = (hnd!=NULL);
    btnAsyncDac1->Enabled = (hnd!=NULL) && chkDacPresent->Checked;
    btnAsyncDac2->Enabled = (hnd!=NULL) && chkDacPresent->Checked;
    btnAsyncAdcFrame->Enabled = (hnd!=NULL) && !threadRunning;

}

/* ���������� ������ ������������ ���� L502 */
void TForm1::refreshDeviceList(void)
{
    uint32_t dev_cnt = 0;
    int32_t res;

    int old_index = cbbSerialList->ItemIndex;

    cbbSerialList->Items->Clear();


    /* ������ �������� ����� ���������� ��������� */
    res  = L502_GetSerialList(NULL, 0, 0, &dev_cnt);
    if ((res>=0) && dev_cnt)
    {
        /* �������� ������ ��� �������� ������ ���� ��������� ��������� */
        t_l502_serial_list list = (t_l502_serial_list) malloc(dev_cnt*L502_SERIAL_SIZE);
        res = L502_GetSerialList(list, dev_cnt, 0, NULL);
        if (res>0)
        {
            /* ��������� �������� ������ � ���� ComboBox'� */
            for (int32_t i=0; i < res; i++)
            {
                cbbSerialList->Items->Add(list[i]);
            }
        }
        /* ����������� ���������� ������ */
        free(list);
    }

    /* ���� ����� ���� ���� ����� - �������� ������ � ��������� ����� � ������ ����� */
    if (res>0)
    {
        /* ���� ��� ������ �������������� ������ - ��������������� ��� */
        cbbSerialList->ItemIndex = (old_index >= 0) && (old_index < res) ? old_index : 0;
    }

    updateControls();

	/* ���� ���� ������ - ������� ��������� � ������ */
	if (res<0)
	{
		MessageDlg("������ ��������� ������ �������: " + String(L502_GetErrorString(res)),
			mtError, TMsgDlgButtons() << mbOK,NULL);
    }
}


/* ��������� ���������� ����� ������ � ������������ � ���������� GUI */
int32_t TForm1::setupParams()
{
    int32_t err = 0;
    uint32_t lch_cnt = cbbLChCnt->ItemIndex+1;
    /* ������� ������������ �������� � ComboBox � ����� ������ ��������� */
    static uint32_t f_mode_tbl[] = {L502_LCH_MODE_COMM, L502_LCH_MODE_DIFF, L502_LCH_MODE_ZERO};
    /* ������� ������������ �������� � ComboBox � ����� ���������� ��� */
    static uint32_t f_range_tbl[] = {L502_ADC_RANGE_10, L502_ADC_RANGE_5, L502_ADC_RANGE_2,
									L502_ADC_RANGE_1, L502_ADC_RANGE_05, L502_ADC_RANGE_02};
	/* ������� ������������ �������� � ComboBox � ����� ��������� ������������� */
	static uint32_t f_sync_tbl[] = {L502_SYNC_INTERNAL, L502_SYNC_EXTERNAL_MASTER,									
									L502_SYNC_DI_SYN1_RISE, L502_SYNC_DI_SYN2_RISE,
									L502_SYNC_DI_SYN1_FALL, L502_SYNC_DI_SYN2_FALL    };

    /* ������������� ���-�� ���������� ������� */
    err = L502_SetLChannelCount(hnd, lch_cnt);
    if (!err)
    {
        err = L502_SetLChannel(hnd,0, cbbLCh1_Channel->ItemIndex,
              f_mode_tbl[cbbLCh1_Mode->ItemIndex], f_range_tbl[cbbLCh1_Range->ItemIndex],0);
    }
    if (!err && (lch_cnt>=2))
    {
        err = L502_SetLChannel(hnd,1, cbbLCh2_Channel->ItemIndex,
              f_mode_tbl[cbbLCh2_Mode->ItemIndex], f_range_tbl[cbbLCh2_Range->ItemIndex],0);
    }
    if (!err && (lch_cnt>=3))
    {
        err = L502_SetLChannel(hnd,2, cbbLCh3_Channel->ItemIndex,
              f_mode_tbl[cbbLCh3_Mode->ItemIndex], f_range_tbl[cbbLCh3_Range->ItemIndex],0);
	}

	/* ����������� �������� ������� ������������� � ������� ����� */
	if (!err) 
		err = L502_SetSyncMode(hnd, f_sync_tbl[cbbSyncMode->ItemIndex]);
	if (!err)
        err = L502_SetSyncStartMode(hnd, f_sync_tbl[cbbSyncStartMode->ItemIndex]);

    /* ����������� ������� ����� � ��� */
    if (!err)
        err = setAdcFreq();

    /* ���������� ��������� � ������ */
    if (!err)
        err = L502_Configure(hnd, 0);

    return err;
}


int32_t TForm1::setAdcFreq()
{
    double f_acq = StrToFloat(edtAdcFreq->Text);
    double f_lch = StrToFloat(edtAdcFreqLch->Text);
    double f_din = StrToFloat(edtDinFreq->Text);
    /* ������������� ��������� ������� �����.
        */
    int32_t err = L502_SetAdcFreq(hnd, &f_acq, &f_lch);
    if (!err)
    {
        /* ��������� �������� ����������, ��� �����������
           ������� �������������� ������� */
        edtAdcFreq->Text = FloatToStr(f_acq);
        edtAdcFreqLch->Text = FloatToStr(f_lch);

        /* ������������� ������� ����������� ����� */
        err = L502_SetDinFreq(hnd, &f_din);
        if (!err)
            edtDinFreq->Text = FloatToStr(f_din);
    }

    return err;
}


int32_t TForm1::setSyncDinStream()
{
    int32_t err;
    /* ��������� ��� ��������� ����� ����������� �����
       � ����������� �� ��������� ������������� */
    if (chkSyncDin->Checked)
        err = L502_StreamsEnable(hnd, L502_STREAM_DIN);
    else
        err = L502_StreamsDisable(hnd, L502_STREAM_DIN);
    return err;
}

void __fastcall TForm1::OnThreadTerminate(TObject *obj)
{
	if (thread->err)
	{
		MessageDlg("���� ������ �������� � �������: " + String(L502_GetErrorString(thread->err)),
			mtError, TMsgDlgButtons() << mbOK,NULL);
	}

    threadRunning = false;

    updateControls();
}



//---------------------------------------------------------------------------
__fastcall TForm1::TForm1(TComponent* Owner)
    : TForm(Owner), hnd(NULL)
{
    refreshDeviceList();
}
//---------------------------------------------------------------------------


void __fastcall TForm1::btnRefreshDeviceListClick(TObject *Sender)
{
    refreshDeviceList();
}
//---------------------------------------------------------------------------

void __fastcall TForm1::btnStartClick(TObject *Sender)
{
    /* ����������� ��� ��������� � ������������ � ���������� ���������� */
    int32_t err = setupParams();
	if (err)
	{
		MessageDlg("������ ��������� ���������� �����: " + String(L502_GetErrorString(err)),
			mtError, TMsgDlgButtons() << mbOK,NULL);
	}

	/* ��������� ���������� ���� ��� */
    if (!err)
        err = L502_StreamsEnable(hnd, L502_STREAM_ADC);
    /* ��������� ���������� ���� � �������� ����� � ����������� �� ������������� */
    if (!err)
        err = setSyncDinStream();


    if (!err)
    {
        /* ���� ������� �� ��������� ������ ������ - ������� */
        if (thread)
        {
            delete thread;
            thread = NULL;
        }
        //�������� ������ ������
        thread = new L502_ProcessThread(true);
        thread->hnd = hnd;

        //�������� ������ ��������� ���������� ��� ����������� ����������� �����
        thread->lchResEdits[0] = edtLCh1_Result;
        thread->lchResEdits[1] = edtLCh2_Result;
        thread->lchResEdits[2] = edtLCh3_Result;
        thread->dinResEdit = edtDin_Result;

        edtLCh1_Result->Text="";
        edtLCh2_Result->Text="";
        edtLCh3_Result->Text="";
        edtDin_Result->Text="";

        /* ������������� ������� �� ������� ���������� ������ (� ���������
            ����� ���������, ���� ����� ���������� ��� ��-�� ������ ��� �����
            ������) */
        thread->OnTerminate = OnThreadTerminate;
        thread->Resume();  //������ ������
        threadRunning = true;

        updateControls();
    }
}
//---------------------------------------------------------------------------

void __fastcall TForm1::btnStopClick(TObject *Sender)
{
    /* ������������� ������ �� ���������� ������ */
    if (threadRunning)
        thread->stop = true;
    btnStop->Enabled = false;
}



/* ������� �������� ���������� � ��������� �������� ������� ��� �������� ���,
    ���� ��� ��� ���� ������� */
void __fastcall TForm1::btnOpenClick(TObject *Sender)
{
    /* ���� ���������� ���� ������� - ������� ����� ��������� � ��������� ��� */
    if (hnd==NULL)
    {
        hnd = L502_Create();
        if (hnd==NULL)
		{
			MessageDlg("������ �������� ��������� ������", mtError, TMsgDlgButtons() << mbOK,NULL);
		}
		else
        {
            /* ��������� ������ � ��������� �������� ������� */
            AnsiString serial = cbbSerialList->Items->Strings[cbbSerialList->ItemIndex];
            int32_t err = L502_Open(hnd, serial.c_str());
			if (err)
			{
				MessageDlg("������ �������� ������: " + String(L502_GetErrorString(err)),
					mtError, TMsgDlgButtons() << mbOK,NULL);
				L502_Free(hnd);
                hnd = NULL;
            }
            else
            {
                t_l502_info info;
                err = L502_GetDevInfo(hnd, &info);
                if (!err)
                {
                    chkDacPresent->Checked = info.devflags & L502_DEVFLAGS_DAC_PRESENT;
                    chkGalPresent->Checked = info.devflags & L502_DEVFLAGS_GAL_PRESENT;
                    chkDspPresent->Checked = info.devflags & L502_DEVFLAGS_BF_PRESENT;

                    edtPldaVer->Text = IntToStr(info.plda_ver);
                    edtFpgaVer->Text = IntToStr((info.fpga_ver>>8)&0xFF) + "."
                                        + IntToStr(info.fpga_ver & 0xFF);

                }
                else
				{
					MessageDlg("������ ��������� ���������� � ������: " + String(L502_GetErrorString(err)),
						mtError, TMsgDlgButtons() << mbOK,NULL);

                    L502_Close(hnd);
                    L502_Free(hnd);
                    hnd = NULL;
                }
            }
        }
    }
    else
    {
        /* ���� ���������� ���� ������� - ��������� */
        closeDevice();
    }
    updateControls();

}


//---------------------------------------------------------------------------

void TForm1::closeDevice()
{
    if (hnd!=NULL)
    {
        /* ���� ������� ����� ����� ������ - �� ������������� ��� */
        if (threadRunning)
        {
            thread->stop=1;
            thread->WaitFor();
        }
        /* �������� ����� � ������� */
        L502_Close(hnd);
        /* ������������ ��������� */
        L502_Free(hnd);
        hnd = NULL;
    }
}
//---------------------------------------------------------------------------

void __fastcall TForm1::FormClose(TObject *Sender, TCloseAction &Action)
{
    closeDevice();
    if (thread)
        delete thread;
}
//---------------------------------------------------------------------------

void __fastcall TForm1::btnAsyncDigOutClick(TObject *Sender)
{
	if (hnd!=NULL)
    {
        uint32_t val = StrToInt(edtAsyncDigOut->Text);
        int32_t err = L502_AsyncOutDig(hnd, val, 0);
        if (err)
		{
			MessageDlg("������ ������������ ������ �� �������� �����: " + String(L502_GetErrorString(err)),
					mtError, TMsgDlgButtons() << mbOK,NULL);
        }
    }
}
//---------------------------------------------------------------------------

void __fastcall TForm1::adcFreqEditChange(TObject *Sender)
{
    if (hnd!=NULL)
    {
        /* ������������� ���-�� ���������� �������, ����� ���������
           ��������� ������� �� ����� */
        int32_t err = L502_SetLChannelCount(hnd, cbbLChCnt->ItemIndex+1);
        if (!err)
            err = setAdcFreq();
		if (err)
		{
			MessageDlg("������ ��������� ������� �����: " + String(L502_GetErrorString(err)),
					mtError, TMsgDlgButtons() << mbOK,NULL);
		}
    }
}
//---------------------------------------------------------------------------

void __fastcall TForm1::chkSyncDinClick(TObject *Sender)
{
    if (hnd!=NULL)
    {
		int32_t err = setSyncDinStream();
		if (err)
		{
			MessageDlg("������ ����������/������� ����������� �����: " + String(L502_GetErrorString(err)),
					mtError, TMsgDlgButtons() << mbOK,NULL);
        }
    }
}
//---------------------------------------------------------------------------

void __fastcall TForm1::btnAsyncDigInClick(TObject *Sender)
{
    if (hnd!=NULL)
    {
        uint32_t din;
        int32_t err = L502_AsyncInDig(hnd, &din);
		if (err)
		{
			MessageDlg("������ ������������ ����� � �������� �����: " + String(L502_GetErrorString(err)),
				mtError, TMsgDlgButtons() << mbOK,NULL);
		}
		else
		{
			edtAsyncDigIn->Text = IntToHex((int)din&0x3FFFF, 5);
        }
    }
}
//---------------------------------------------------------------------------

void __fastcall TForm1::btnAsyncDac1Click(TObject *Sender)
{
    if (hnd!=NULL)
    {
        double val = StrToFloat(edtAsyncDac1->Text);;
        int32_t err;
        err = L502_AsyncOutDac(hnd, L502_DAC_CH1, val, L502_DAC_FLAGS_CALIBR |
                                                        L502_DAC_FLAGS_VOLT);
		if (err)
		{
			MessageDlg("������ ������ �� ���: " + String(L502_GetErrorString(err)),
				mtError, TMsgDlgButtons() << mbOK,NULL);
        }
	}

}
//---------------------------------------------------------------------------

void __fastcall TForm1::btnAsyncDac2Click(TObject *Sender)
{
    if (hnd!=NULL)
    {
        double val = StrToFloat(edtAsyncDac2->Text);;
        int32_t err;
        err = L502_AsyncOutDac(hnd, L502_DAC_CH2, val, L502_DAC_FLAGS_CALIBR |
                                                        L502_DAC_FLAGS_VOLT);
		if (err)
		{
			MessageDlg("������ ������ �� ���: " + String(L502_GetErrorString(err)),
				mtError, TMsgDlgButtons() << mbOK,NULL);
		}
    }
}
//---------------------------------------------------------------------------

void __fastcall TForm1::btnAsyncAdcFrameClick(TObject *Sender)
{
    if (hnd!=NULL)
    {
        uint32_t lch_cnt;
        /* ����������� ���������� ������� ��� */
        int32_t err = setupParams();
        if (!err)
            err = L502_GetLChannelCount(hnd, &lch_cnt);

		if (err)
		{
			MessageDlg("������ ��������� ���������� ���: " + String(L502_GetErrorString(err)),
				mtError, TMsgDlgButtons() << mbOK,NULL);
		}

        if (!err)
        {
            /* � ������� ���������� 3 ������ - ������� ������ ��� ������
               ������� �� ������ ����� */
            double adc_data[3];
            err = L502_AsyncGetAdcFrame(hnd, L502_PROC_FLAGS_VOLT, 1000, adc_data);
			if (err)
			{
				MessageDlg("������ ������ ����� ���: " + String(L502_GetErrorString(err)),
							mtError, TMsgDlgButtons() << mbOK,NULL);
			}
			else
			{
                edtLCh1_Result->Text = FloatToStrF(adc_data[0], ffFixed, 4, 8);
                if (lch_cnt>=2)
                    edtLCh2_Result->Text = FloatToStrF(adc_data[1], ffFixed, 4, 8);
                else
                    edtLCh2_Result->Text ="";

                if (lch_cnt>=3)
                    edtLCh3_Result->Text = FloatToStrF(adc_data[2], ffFixed, 4, 8);
                else
                    edtLCh3_Result->Text = "";
            }
        }
    }
}
//---------------------------------------------------------------------------


void __fastcall TForm1::cbbSerialListChange(TObject *Sender)
{
    //refreshDeviceList();
}
//---------------------------------------------------------------------------


void __fastcall TForm1::cbbLChCntChange(TObject *Sender)
{
    if (cbbLChCnt->ItemIndex < 0)
        cbbLChCnt->ItemIndex = 0;
}
//---------------------------------------------------------------------------


